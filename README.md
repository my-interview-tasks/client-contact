ZADANIE:

Napisz aplikację - formularz kontaktowy dla klienta. W formularzu powinny być dostępne pola:
Imię, nazwisko, adres email, adres korespondencyjny (jedno pole), telefon kontaktowy, cel kontaktu (pole tekstowe).
Pola imię i nazwisko są wymagane. Jedno z pól: email lub telefon kontaktowy są wymagane.
Aplikacja po zbudowaniu powinna trafić do jednego pliku .war lub .jar w katalogu target projektu.
Aplikacja powinna być możliwa do uruchomienia na kontenerze Tomcat.

Wymagania techniczne:
Backend: java 11, spring-boot 2.x + baza H2 w pamięci. Przy komunikacji z bazą danych użyj: JPA, JDBC lub MyBatis, napisz co najmniej jeden test jednostkowy + jeden test który podniesie kontekst spring-owy.
Frontend: dowolna technologia

Przy pisaniu aplikacji główny nacisk połóż na czytelność kodu