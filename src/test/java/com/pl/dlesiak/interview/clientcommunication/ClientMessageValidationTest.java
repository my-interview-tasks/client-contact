package com.pl.dlesiak.interview.clientcommunication;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ClientMessageValidationTest {

    private Validator validator;

    @BeforeEach
    public void setUp(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
    }

    @Test
    public void fail_validation_when_invalid_contact_information(){
        ClientMessage message = clientMessageWithInvalidContact();

        Set<ConstraintViolation<ClientMessage>> violations = validator.validate(message);

        assertEquals(1,violations.size());

        //todo equivalent of every() in groovy?
        for (ConstraintViolation<ClientMessage> violation : violations)
             assertEquals("Either phone or email is required!", violation.getMessage());
    }


    @ParameterizedTest
    @MethodSource("provideValidClientMessages")
    public void success_validation_when_valid_contact_information(ClientMessage clientMessage){

        Set<ConstraintViolation<ClientMessage>> violations = validator.validate(clientMessage);

        assertTrue(violations.isEmpty());
    }

    private static Stream<Arguments> provideValidClientMessages(){
        return Stream.of(
                Arguments.of(clientMessageWithEmail()),
                Arguments.of(clientMessageWithPhone())
        );
    }

    //todo
    private static ClientMessage clientMessageWithInvalidContact(){
        ClientMessage message = new ClientMessage();
        message.setFirstName("MockName");
        message.setLastName("MockLastName");
        return message;
    }

    private static ClientMessage clientMessageWithPhone(){
        ClientMessage clientMessage = clientMessageWithInvalidContact();
        clientMessage.setPhone("123414");
        return clientMessage;
    }

    private static ClientMessage clientMessageWithEmail(){
        ClientMessage clientMessage = clientMessageWithInvalidContact();
        clientMessage.setEmail("email@email.pl");
        return clientMessage;
    }


}