package com.pl.dlesiak.interview.clientcommunication;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ContactServiceIntegrationTest {

    @Autowired
    private MessageService messageService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void viewClientMessageForm() throws Exception {
        mockMvc.perform(get("/contact"))
                .andExpect(status().isOk())
                .andExpect(view().name("contact"))
                .andExpect(model().attributeExists("clientMessage"));
    }

    @Test
    public void sendNewClientMessage() throws Exception {

        MockHttpServletRequestBuilder request =
                post("/contact")
                        .param("firstName", "Jan")
                        .param("lastName", "Nowak")
                        .param("address", "Ul. Wiejska")
                        .param("email", "email@email.pl")
                        .param("phone", "12345")
                        .param("message", "Hello!");

        mockMvc.perform(request)
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/confirmation?name=Jan"));


        List<ClientMessage> result = messageService.getMessages();

        assertEquals(1, result.size());

        assertThat(result.get(0))
                .isEqualToIgnoringGivenFields(clientMessage(), "id");
    }

    private static ClientMessage clientMessage() {
        ClientMessage message = new ClientMessage();
        message.setId(1);
        message.setFirstName("Jan");
        message.setLastName("Nowak");
        message.setPhone("12345");
        message.setEmail("email@email.pl");
        message.setMessage("Hello!");
        message.setAddress("Ul. Wiejska");
        return message;
    }


}