package com.pl.dlesiak.interview.clientcommunication;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "client_message")
@ValidContact
public class ClientMessage {

    @Id
    @GeneratedValue
    private long id;
    @NotBlank(message = "First name is required!")
    @Column(name = "first_name")
    private String firstName;
    @NotBlank(message = "Last name is required!")
    @Column(name = "last_name")
    private String lastName;
    @Email(message = "Invalid email")
    @Column(name = "email")
    private String email;
    @Column(name = "mail_address")
    private String address;
    @Column(name = "phone")

    private String phone;
    @Column(name = "message")
    private String message;

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getMessage() {
        return message;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClientMessage{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
