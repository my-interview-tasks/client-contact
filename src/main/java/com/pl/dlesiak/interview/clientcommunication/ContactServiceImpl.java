package com.pl.dlesiak.interview.clientcommunication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
class ContactServiceImpl implements ContactService, MessageService {

    private final ClientMessageRepository clientMessageRepository;

    @Autowired
    public ContactServiceImpl(ClientMessageRepository clientMessageRepository) {
        this.clientMessageRepository = clientMessageRepository;
    }

    @Transactional
    @Override
    public void sendMessage(ClientMessage clientMessage) {
        clientMessageRepository.save(clientMessage);
    }

    @Override
    public List<ClientMessage> getMessages() {
        return clientMessageRepository.findAll();
    }

    @Override
    public void delete(long id) {
        clientMessageRepository.deleteById(id);
    }
}
