package com.pl.dlesiak.interview.clientcommunication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ClientMessageRepository extends JpaRepository<ClientMessage, Long> {
}
