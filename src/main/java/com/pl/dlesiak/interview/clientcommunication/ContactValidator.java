package com.pl.dlesiak.interview.clientcommunication;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ContactValidator implements ConstraintValidator<ValidContact, ClientMessage> {


    public void initialize(ValidContact constraint) {
    }

    @Override
    public boolean isValid(ClientMessage clientMessage, ConstraintValidatorContext constraintValidatorContext) {
        return clientMessage.getPhone() != null || clientMessage.getEmail() != null;
    }


}
