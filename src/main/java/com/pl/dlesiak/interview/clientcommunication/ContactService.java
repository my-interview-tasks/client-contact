package com.pl.dlesiak.interview.clientcommunication;

public interface ContactService {

    void sendMessage(ClientMessage clientMessage);

}
