package com.pl.dlesiak.interview.clientcommunication;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ContactValidator.class)
public @interface ValidContact {

    String message() default "Either phone or email is required!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
