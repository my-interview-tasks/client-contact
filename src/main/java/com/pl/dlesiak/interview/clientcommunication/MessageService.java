package com.pl.dlesiak.interview.clientcommunication;

import java.util.List;

public interface MessageService {
    List<ClientMessage> getMessages();

    void delete(long id);
}
