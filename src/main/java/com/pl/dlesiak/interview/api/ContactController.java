package com.pl.dlesiak.interview.api;

import com.pl.dlesiak.interview.clientcommunication.ClientMessage;
import com.pl.dlesiak.interview.clientcommunication.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@org.springframework.stereotype.Controller
public class ContactController {

    private final ContactService contactService;

    @Autowired
    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/")
    public String main() {
        return "index";
    }

    @GetMapping("/contact")
    public String showForm(Model model) {
        model.addAttribute("clientMessage", new ClientMessage());
        return "contact";
    }

    @PostMapping("/contact")
    public String submitForm(@Valid @ModelAttribute("clientMessage") ClientMessage clientMessage, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "contact";
        }
        contactService.sendMessage(clientMessage);
        redirectAttributes.addAttribute("name", clientMessage.getFirstName());
        return "redirect:/confirmation"; //todo redirect
    }

    @GetMapping("/confirmation")
    public String confirmation(Model model, @ModelAttribute("name") String name) {
        model.addAttribute("name", name);
        return "confirmation";
    }

    @ExceptionHandler(Exception.class)
    public String handleAllException(Exception ex) {
        return "redirect:/error";
    }
}
