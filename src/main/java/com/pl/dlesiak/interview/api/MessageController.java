package com.pl.dlesiak.interview.api;

import com.pl.dlesiak.interview.clientcommunication.ClientMessage;
import com.pl.dlesiak.interview.clientcommunication.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;

@Controller
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/messages")
    public String showMessages(Model model) {
        Collection<ClientMessage> messages = messageService.getMessages();
        model.addAttribute("messages", messages);
        return "messages";
    }

    @DeleteMapping("/message/{id}")
    public String deleteMessage(@PathVariable(name = "id") long id) {
        messageService.delete(id);
        return "messages";
    }

}
